# Nixu Challenge #


### password_verifier ###
password_verifier tiedoston ajaminen onnistui sittenkin, kun tajusi vaihtaa tiedoston headerin python3.4:ään eikä python 2.7:ään! Seuraavaksi ajettavissaolevan tiedoston purkuyritys Decompyle++ softalla. Tuloksena oli lähes ajettava python skripti. Tein hiukan puretun koodin tutkimista ja muovaamista ajettavissa olevaksi skriptiksi, jotta selviäisi mitä koodi oikeasti tekee. Käsitys syntyi, mutta loppu jää hiukan auki enkä onnistu keksimään mikä syöte skriptille pitäisi antaa jotta tuloksena olisi muuta kuin "bad".

Tiedostolistausta

* password_verifier: originaali tiedosto
* password_verifier.pyc: alkuperäinen hex header (160d) muutettu python3.4 headeriksi (ee0c)
* password_verifier.pyc.assembly: Decompyle++ softan tulostama purettu assembly

* x.py.decompyle: Decompyle++ softan purkama raaka python koodi
* x.py: Decompyle++ softan purkama python koodi, muokattu ajettavaksi
* x_jj.py: oma yritys muokata ja tutkia purettua python koodia järkevästi toimivaksi

* x.pyc: x.py käännettynä takaisin binääriksi
* x.pyc.decompyle: x.pyc Decompyle++ purettuna python koodiksi vertailua varten
* x.pyc.assembly: x.pyc Decompyle++ purettuna assemblyyn

### secret.txt ###
key.jpg tarkemman googlettamisen tulos vie näköjään sivulle, jolla neuvotaan ratkaisu Project Euler 59 -tehtävään. Kyseessä XOR kryptaus. Sitä voisi yrittää tähän secret.txt tiedostoon..? Koitin myös base64 dekoodata tiedostoa, mutte tuloksena vaan tuntematonta dataa, joten se tuskin on oikea ratkaisu..?

* secret.txt: originaali tiedosto

* secret.txt.base64decoded: secret.txt base64 dekoodattuna. Tuloksena mystistä binääriä. Luultavasti siis turhaa...

* xorkoe.py: vielä keskeneräinen skripti, jolla yrittää XOR menetelmää.

xorkoe.py tuloksia. Toistaiseksi skripti vasta tulostaa ascii numerokoodit ja niiden binäärit. Tavoitteena tämä:

* originaali teksti --> originaali ascii merkit -->  originaali binääri
* salasana teksti --> salasana ascii merkit --> salasana binääri --> XOR (originaali binääri) --> tulos ascii merkit --> tulos tekstinä

skriptin tuloksia tähän mennessä:

* secret.txt.ascii: secret.txt merkit ascii numerokoodeina
* secret.txt.binary: secret.txt ascii numerokoodit binäärinä


