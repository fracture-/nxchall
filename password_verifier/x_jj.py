#! /usr/bin/python3

b = 43															# mahdollinen vinkki salasanan pituuteen? max 44 merkkiä
x = input().encode('utf-8')										# muunna input utf-8 koodeiksi
j = 291															# mystinen vakio...

print ("")
print ("b: " + str(b))
print ("x: " + str(x))
print ("")
print ("")

try:
	print (">>> try, silmukka")
	print ("")
	while b:
		print ("-----")
		print ("b: " + str(b))
		print ("j1: " + str(j))
		
		# muokkaa j ascii-numeron ja vakion perusteella
		print (">>> j = 351 * " + str(j) + " + " + str(x[43-b]))
		j = 351 * j + x[43 - b]
		
		print ("j2: " + str(j))
		b -= 1

except:
	print ("")
	print ("")
	print (">>> except!")
	print ("")
	print ("x: " + str(x))
	print (">>> x = " + str(j) + " modulo " + "64338917")
	x = j % 64338917
	print ("x: " + str(x))
	print ("")

print ("")
print (">>> continue")
print (">>> x XOR " + str(j) + " modulo 0x2B3EA2D3303L")

# mystinen L 0x2B3EA2D3303L lopussa sotkee asioita..? koitetaan ilman sitä.
# L näyttäisi ilmestyvän decompyle tulokseen, vaikka koodin kääntää ilman L-kirjainta.
# Sen voinee siis jättää pois...
x |= j % 0x2B3EA2D3303											

# 2B3EA2D3303L base64 dekoodattuna, hexamuodossa: 
# d81d c403 60f7 df4d cb

print ("x: " + str(x))

# mystinen osuus jonka toiminta ei vielä ihan selvillä...

#if x:
#    pass
#1('good')														# viittaa ehkä funktioon nimeltään 1? 
