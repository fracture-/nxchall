#! /usr/bin/python3

############################################################
# XOR työkalu nixu haasteeseen, -j.j.
############################################################

DESC = "XOR työkalu nixu haasteeseen, -j.j."


############################################################
#  imports, vars
############################################################

import argparse

input_chars = []				# original characters
input_ascii = []				# ascii codes of characters
input_binary = []				# binary values of ascii codes
password_chars = []				# password original chars
password_ascii = []				# asvii codes of password
password_binary = []			# binary of password


############################################################
#  functions
############################################################

# open & process input file
def open_inputfile(newinputfile, input_chars):

	# try to open file
	try:
		with open (newinputfile) as inputfile:

			# with success, read data and create list, character by character
			while True:
				# read single character
				c = inputfile.read(1)
				if not c:
					#print ("end of file")
					break
				#print ("c: " + str(c))
				input_chars.append(c)


	# throw exception when failing
	except Exception as e:
		print ("read error: ")
		print (e)

# get password
def get_password(password):
	# read passwords characters
	password_chars = list(password)
	return password_chars

# generate ascii
def gen_ascii(input_chars, input_ascii):

	for c in input_chars:
		# append ascii code to ascii list
		input_ascii.append(ord(c))

# generate binary
def gen_binary(input_ascii, input_binary):
	
	for a in input_ascii:
		# append binary to binary list
		input_binary.append(bin(a))

# print ascii
def print_ascii(input_ascii):
	ascii = ""
	for a in input_ascii:
		# '{0:03}' 		= place variable into a string, start at pos 0, add formatting, 3 digits, zero-pad left
		# .format(a) 	= which variable to format
		ascii = ascii + '{0:03}'.format(a) + " "
	print (str(ascii))

# print binary
def print_binary(input_binary):
	binary = ""
	for b in input_binary:
		# '{0:08}' 		= place variable into a string, start at pos 0, add formatting, 8 digits, zero-pad left
		# .format(b) 	= which variable to format
		#binary = binary + '{0:08b}'.format(b) + " "
		#print ('{0:08b}'.format(int(b)))
		#print (b[2:].zfill(8))
		binary = binary + b[2:].zfill(8) + " "
		#binary = binary + str(b)[2:] + " "
	print (str(binary))
		


############################################################
#  args
############################################################

# create argparser
parser = argparse.ArgumentParser(description=DESC)

# add arguments
parser.add_argument('-i', dest='inputfile', help='input file')
parser.add_argument('-p', dest='password', help='password')
parser.add_argument('-ai', dest='showascii_i', action='store_true', help='show ascii of input')
parser.add_argument('-bi', dest='showbinary_i', action='store_true', help='show binary of input')
parser.add_argument('-ap', dest='showascii_p', action='store_true', help='show ascii of password')
parser.add_argument('-bp', dest='showbinary_p', action='store_true', help='show binary of password')

#execute argparse
args = parser.parse_args()


############################################################
#  functionality
############################################################

# if input
if args.inputfile:
	# open file, create character list
	open_inputfile(args.inputfile, input_chars)
	# create ascii list
	gen_ascii(input_chars, input_ascii)
	# create binary list
	gen_binary(input_ascii, input_binary)
	# print (input_binary)

	# print ascii output if requested
	if args.showascii_i:
		print_ascii(input_ascii)

	# print binary output if requested
	if args.showbinary_i:
		print_binary(input_binary)

	# get password
	if args.password:
		# create password character list
		password_chars = get_password(args.password)
		# create password ascii list
		gen_ascii(password_chars, password_ascii)
		# create password binary list
		gen_binary(password_ascii, password_binary)

		#print password ascii if requested
		if args.showascii_p:
			print_ascii(password_ascii)
		#print password binary if requested
		if args.showbinary_p:
			print_binary(password_binary)






# if not, show help
else:
	parser.print_help()